
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vshm.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.vshm.lib.io.io;
import de.vshm.lib.json.JsonArray;
import de.vshm.lib.json.JsonObject;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.logging.Level;

/**
 *
 * @author 006020x
 */
public abstract class baseapphandler extends actionbase implements HttpHandler {
    private static final String CALL_URL_ = "call URL: ";
    private static final String HTTP_METHOD = "POST";
    private static final String RESPONSECODE_ = "responsecode: ";
    private static final String HTTP = "http://";
    private static final String HANDLERAPP = "handler:app:";
    private static final String HANDLERAPP_URI_ = "handler:app:URI: ";
    private static final String THE_SERVER_APP = "the server app";
    private static final String CONTENTTYPE = "Content-Type";
    private static final String APPLICATIONJSONCHARSET_UTF8 = "text/plain;charset=utf-8";
    private static final String BEARBEITETE_URL_ = "bearbeitete url: ";
    private static final String UT_F8 = "UTF-8";
    private static final String PARAMS = "params";
    private static final String HANDLERAPP_DATA_ = "handler:app:DATA: ";
    private static final String REQUEST = "request";
    private static final String RESPONSE = "response";
    private static final String __FEHLER_BEIM__AKTIONSAUFRUF = " !!!!!Fehler beim Aktionsaufruf!!!!!";
    private static final String AKTION_NICHT_GEFUNDEN = "aktion nicht gefunden!";
    private static final String APIKEY = "apikey";
    private static final String NO_APIKEY_DEFINED = "no apikey defined";
    private static final String ERROR = "error";
    private static final String APIKEY_NOT_VALID = "apikey not valid";

    public HttpExchange httpexchange;
    public boolean fehler = false;
    public URLClassLoader urlcl = null;

    public boolean isFehler() {
        return fehler;
    }

    public void setFehler(boolean fehler) {
        this.fehler = fehler;
    }

    public URLClassLoader getUrlcl() {
        return urlcl;
    }

    public void setUrlcl(URLClassLoader urlcl) {
        this.urlcl = urlcl;
    }

    public HttpExchange getHttpexchange() {
        return httpexchange;
    }

    public String getServerurl() {
        return serverurl;
    }
    public String serverurl;

    @Override
    public void handle(HttpExchange he) throws IOException {

        try {
            this.httpexchange = he;
            this.serverurl = HTTP + httpexchange.getLocalAddress().getAddress().getHostAddress() + ":" + httpexchange.getLocalAddress().getPort();

            byte[] response = null;
            logger.getInstance().getLogger().log(Level.INFO, HANDLERAPP);
            logger.getInstance().getLogger().log(Level.INFO, HANDLERAPP_URI_ + he.getRequestURI());
            JsonObject data = null;
            String resp = THE_SERVER_APP;
            //es werden nur javascript-dinge gebraucht. damit müssen die objekte gemacht werden
            
            he.getResponseHeaders().set(CONTENTTYPE, APPLICATIONJSONCHARSET_UTF8);
            this.setConfig((JsonObject) he.getHttpContext().getAttributes().get("config"));
            this.urlcl = (URLClassLoader) he.getHttpContext().getAttributes().get("ucl");
            String urie = he.getRequestURI().getPath();
            int fragezeichen = urie.indexOf("?");
            if (fragezeichen > 0) {
                urie = urie.substring(0, fragezeichen);
            }
            urie = urie.replace(he.getHttpContext().getPath(), "");
            logger.getInstance().getLogger().log(Level.INFO, BEARBEITETE_URL_ + urie);
            
            // Daten aus dem Body holen
            try {
                String s = io.getInputStreamAsString(he.getRequestBody()).trim();
                data = JsonObject.readFrom(s);
            } catch (Exception iOException) {
                iOException.printStackTrace();
                data = new JsonObject();
            }
            
            //urlparameter rausholen, die mit "/" getrennt sind. key/value-pairs nicht!
            
            String[] arrParams = urie.split("/");
            JsonArray jarr = new JsonArray();
            for (int i = 1; i < arrParams.length; i++) {
                try {
                    jarr.add(URLDecoder.decode(arrParams[i], UT_F8));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            data.add(PARAMS, jarr);
            //body und urlparameter, KEINE Queryparameter
            this.setData(data);
            logger.getInstance().getLogger().log(Level.INFO, HANDLERAPP_DATA_ + data.toString());
            try {
                JsonObject ret = new JsonObject();
                ret.add(RESPONSE, this.execute());
                ret.add(REQUEST, data);
                resp = ret.toString();
            } catch (Exception exception) {
                exception.printStackTrace();
                resp = exception.getMessage() + __FEHLER_BEIM__AKTIONSAUFRUF;
            }
            
            response = resp.getBytes();
            he.sendResponseHeaders(200, response.length);
            try (OutputStream os = he.getResponseBody()) {
                os.write(response);
                os.flush();
            }
            he.close();
        } catch (Exception exception) {
            byte[] response = AKTION_NICHT_GEFUNDEN.getBytes();
            he.sendResponseHeaders(200, response.length);
            try (OutputStream os = he.getResponseBody()) {
                os.write(response);
                os.flush();
            }
            he.close();
        }
    }

    public JsonObject execute() {

        JsonObject ret = new JsonObject();
        JsonObject inp = this.getData();
        this.fehler = true;
        if (!inp.names().contains(APIKEY)) {
            ret.add(ERROR, NO_APIKEY_DEFINED);
            return ret;
        }
        if (!this.getConfig().get(APIKEY).asArray().values().contains(inp.get("apikey"))) {
            ret.add(ERROR, APIKEY_NOT_VALID);
            return ret;

        }
        this.fehler = false;
        return ret;
    }

    public JsonObject jsonhttp(String url, JsonObject inhalt) {

        try {
            URL obj = new URL(url);
            logger.getInstance().getLogger().info(CALL_URL_ + url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(HTTP_METHOD);
            con.setDoOutput(true);
            if (inhalt != null) {
                DataOutputStream das = new DataOutputStream(con.getOutputStream());
                das.write(inhalt.toString().getBytes());
                das.flush();
                das.close();
            }
            logger.getInstance().getLogger().info(RESPONSECODE_ + con.getResponseCode());
            return (JsonObject.readFrom(io.getInputStreamAsString(con.getInputStream())));
        } catch (IOException iOException) {
            logger.getInstance().getLogger().log(Level.ALL, "fehler-jsonhttp",iOException);

            return null;
        }

    }

}
