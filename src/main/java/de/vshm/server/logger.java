/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vshm.server;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import de.vshm.lib.io.io;
import de.vshm.lib.json.JsonObject;
/**
 *
 * @author 006020x
 */
public class logger {

    private Logger l;
    private JsonObject configdb = JsonObject.readFrom(io.getFileAsString("./cfg/config.json"));

    private logger() {
        try {
            l = Logger.getAnonymousLogger();
            FileHandler fh = new FileHandler(configdb.get("server").asObject().get("logfile").asString());
            l.addHandler(fh);
            fh.setFormatter(new SimpleFormatter());

        } catch (SecurityException securityException) {
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Logger getLogger() {
        return l;
    }

    public static logger getInstance() {
        return loggerHolder.INSTANCE;
    }

    private static class loggerHolder {

        private static logger INSTANCE = new logger();

    }
}
