/*
 
SSLContext ssl = SSLContext.getInstance("TLS");
KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
KeyStore store = KeyStore.getInstance("JKS");
store.load(new FileInputStream(keystoreFile),keyPass.toCharArray());
keyFactory.init(store, keyPass.toCharArray());
TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
trustFactory.init(store);
ssl.init(keyFactory.getKeyManagers(),
trustFactory.getTrustManagers(), new SecureRandom());
HttpsConfigurator configurator = new HttpsConfigurator(ssl);
HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(hostname, port), port);
httpsServer.setHttpsConfigurator(configurator);
HttpContext httpContext = httpsServer.createContext(uri);
httpsServer.start();
endpoint.publish(httpContext);

For client, be sure you do this:
System.setProperty("javax.net.ssl.trustStore", "path");
System.setProperty("javax.net.ssl.keyStore", "password");
System.setProperty("javax.net.ssl.keyStorePassword", "password");
System.setProperty("javax.net.ssl.keyStoreType", "JKS");
//done to prevent CN verification in client keystore
HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
   @Override
   public boolean verify(String hostname, SSLSession session) {
     return true;
   }
});

 */
package de.vshm.server;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import de.vshm.lib.io.io;
import de.vshm.lib.json.JsonObject;

/**
 *
 * @author 006020x
 */
public class httpserver {

    public HttpServer http;
    public int port = 8888;
    public JsonObject configdb = new JsonObject();

    /**
     * initialisierung des servers
     */
    public void init() {
        /**
         * Die ganzen Bundles laden
         */
        try {

            String configdb_pfad = "./cfg/config.json";
            logger.getInstance().getLogger().log(Level.INFO, "Lese Config: " + configdb_pfad);
            configdb = JsonObject.readFrom(io.getFileAsString(configdb_pfad));
            port = configdb.get("server").asObject().get("port").asInt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * hier erfolgt der start des servers
     */
    public void start() {
        URLClassLoader ucl = urlclassloader();
        try {
            LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
            ThreadPoolExecutor executor = new ThreadPoolExecutor(
                    configdb.get("server").asObject().get("minthread").asInt(),
                    configdb.get("server").asObject().get("maxthread").asInt(), 1, TimeUnit.MINUTES, queue);
            http = HttpServer.create();
            http.setExecutor(executor);

            reghandler(ucl);

            http.bind(new InetSocketAddress(port), 0);
            logger.getInstance().getLogger().log(Level.INFO, "Server gebunden an Port {0}", port);
            http.start();
            logger.getInstance().getLogger().log(Level.INFO, "Server gestartet ");

        } catch (IOException ex) {
            logger.getInstance().getLogger().log(Level.SEVERE, null, ex);
        }
    }

    private void reghandler(URLClassLoader ucl) {
        Iterator<JsonObject.Member> it = configdb.get("handler").asObject().iterator();
        while (it.hasNext()) {
            JsonObject.Member kontext = it.next();
            try {
                /**
                 * Die handler werden aus der Datenbank gelesen und dann dynamisch registriert.
                 */
                HttpContext hc = http.createContext(kontext.getName(),
                        (HttpHandler) ucl.loadClass(kontext.getValue().asString()).newInstance());
                // config und urlclassloader mit rübergeben
                hc.getAttributes().put("config", configdb);
                hc.getAttributes().put("ucl", ucl);
                logger.getInstance().getLogger().log(Level.INFO, "Handler /{0} registriert",
                        kontext.getValue().asString());
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                logger.getInstance().getLogger().log(Level.SEVERE, null, ex);
            }
        }
    }

    private URLClassLoader urlclassloader() {
        ArrayList<URL> urls = new ArrayList<URL>();
        Iterator<JsonObject.Member> jarit = configdb.get("jars").asObject().iterator();
        int jaranz = 0;
        while (jarit.hasNext()) {
            JsonObject.Member jar = jarit.next();
            jaranz++;
            try {
                URL u = new File(jar.getValue().asString()).toURI().toURL();
                urls.add(u);
            } catch (Exception ex) {
                logger.getInstance().getLogger().log(Level.SEVERE, null, ex);
            }
        }
        URL[] a = new URL[jaranz];
        a = urls.toArray(a);
        URLClassLoader ucl = new URLClassLoader(a);
        return ucl;
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("file.encoding"));

        httpserver server = new httpserver();
        server.init();
        server.start();

    }
}
