package de.vshm.lib.geld;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

public abstract class abstractnumber {

    protected BigDecimal gw;
    protected int nk = 0;
    protected DecimalFormat df
            = (DecimalFormat) DecimalFormat.getInstance(Locale.GERMAN);

    /**
     * Method abstractnumber.
     *
     * @param wert
     * @param pattern
     * @param nachkomma
     */
    public abstractnumber(String wert, String pattern, int nachkomma) {
        nk = nachkomma;
        df.setMinimumFractionDigits(nachkomma);
        df.setMaximumFractionDigits(nachkomma);
        df.applyLocalizedPattern(pattern);
        try {
            gw = new BigDecimal(df.parse(wert).doubleValue());
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        } catch (ParseException ex) {

            gw = new BigDecimal(0.0);
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    public abstractnumber(double wert, String pattern, int nachkomma) {

        nk = nachkomma;
        df.setMinimumFractionDigits(nachkomma);
        df.setMaximumFractionDigits(nachkomma);
        df.applyLocalizedPattern(pattern);
        try {
            gw = new BigDecimal(wert);
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        } catch (Exception ex) {
            gw = new BigDecimal(0.0);
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    /**
     * Method getvalue.
     *
     * @return String
     */
    public String getvalue() {
        return df.format(gw.doubleValue());
    }

    /**
     * Method getdblvalue.
     *
     * @return double
     */
    public double getdblvalue() {
        return gw.doubleValue();
    }

    public int getintvalue() {
        return gw.intValue();
    }

    /**
     * Method setvalue.
     *
     * @param wert
     */
    public void setvalue(String wert) {
        try {
            gw = new BigDecimal(df.parse(wert).doubleValue());
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        } catch (Exception ex) {
            gw = new BigDecimal(0.0);
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    /**
     * Method setvalue.
     *
     * @param wert
     */
    public void setvalue(double wert) {
        try {
            gw = new BigDecimal(wert);
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        } catch (Exception ex) {
            gw = new BigDecimal(0.0);
            gw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    /**
     * Method add.
     *
     * @param val
     */
    public void add(String val) {
        try {
            BigDecimal tgw = new BigDecimal(df.parse(val).doubleValue());
            tgw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
            gw = gw.add(tgw);
        } catch (ParseException ex) {
        }
    }

    /**
     * Method sub.
     *
     * @param val
     */
    public void sub(String val) {
        try {
            BigDecimal tgw = new BigDecimal(df.parse(val).doubleValue());
            tgw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
            gw = gw.subtract(tgw);
        } catch (Exception ex) {
        }
    }

    /**
     * Method mul.
     *
     * @param val
     */
    public void mul(String val) {
        try {
            BigDecimal tgw = new BigDecimal(df.parse(val).doubleValue());
            tgw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
            gw = gw.multiply(tgw);
        } catch (Exception ex) {
        }
    }

    /**
     * Method div.
     *
     * @param val
     */
    public void div(String val) {
        try {
            BigDecimal tgw = new BigDecimal(df.parse(val).doubleValue());
            tgw.setScale(nk, BigDecimal.ROUND_HALF_EVEN);
            gw = gw.divide(tgw, BigDecimal.ROUND_HALF_EVEN);
        } catch (Exception ex) {
        }
    }

    public String toString() {
        return this.getvalue();
    }

    public BigDecimal getBigDecimal() {
        return gw;
    }
}
