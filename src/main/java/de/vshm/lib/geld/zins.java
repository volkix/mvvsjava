package de.vshm.lib.geld;

public class zins extends abstractnumber {

    /**
     * Method zins.
     *
     * @param wert
     */
    public zins(String wert) {
        super(wert, "#0,000", 3);
    }

    public zins(double wert) {
        super(wert, "#0,000", 3);
    }

}
