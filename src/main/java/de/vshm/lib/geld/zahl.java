package de.vshm.lib.geld;

public class zahl extends abstractnumber {
	/**
	 * Method zahl.
	 * 
	 * @param wert
	 * @param trenner
	 */
	static String s = "0000000000";

	public zahl(String wert, boolean trenner) {
		super(wert, trenner ? "###.###.##0" : "########0", 0);
	}

	/**
	 * Method zahl.
	 * 
	 * @param wert
	 */
	public zahl(String wert) {
		super(wert, "###.###.##0", 0);
	}

	public zahl(String wert, String nichts) {

		super(wert, wert.length() > s.length() ? "0" : s.substring(0, wert.length()), 0);

	}

}
