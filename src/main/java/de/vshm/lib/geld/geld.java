package de.vshm.lib.geld;

public class geld extends abstractnumber {

    /**
     * Method geld.
     *
     * @param wert
     */
    public geld(String wert) {
        super(wert, "##.###.##0,00", 2);
    }

    public geld(double wert) {
        super(wert, "##.###.##0,00", 2);
    }


}

