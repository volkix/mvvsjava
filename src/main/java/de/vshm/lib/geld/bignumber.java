/*
 * @(#)bignumber.java 1.00 21.05.2003
 *
 * Copyright (c) 2003, BHW Bausparkasse AG 
 * Lubahnstr. 2, 31789 Hameln,
 * Germany All Rights Reserved.
 *
 * 21.05.2003 vschulte  Vers. 1.0     created
 */
package de.vshm.lib.geld;

/**
 * @author 006020x
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class bignumber extends abstractnumber {
    /**
     * Constructor for bignumber.
     * @param wert
     */
    public bignumber(String wert) {
        super(wert, "#.###.###.##0,00000000", 8);
    }
    /**
     * Constructor for bignumber.
     * @param wert
     */
    public bignumber(double wert) {
        super(wert, "#.###.###.##0,00000000", 8);
    }
}
