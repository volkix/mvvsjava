/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package de.vshm.lib.xml;


import java.io.File;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 *
 * @author 006020x
 */
public class xmlParser {

    /**
     * Parsen einer XML-Datei und Rückgabe eines Document-Objektes
     * @param datei
     * @return 
     */
    public static Document parse(File datei) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            
            
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            return db.parse(datei);
        } catch (Exception ex) {
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * Erstellen eines neuen Dom-Objektes
     * @return 
     */
    public static Document newDoc() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            return db.newDocument();
        } catch (Exception ex) {
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * Schreiben eines DOM-Objektes in eine Datei
     * @param d
     * @param f 
     */
    public static void writeDoc(Document d, Result r) {
        try {
            TransformerFactory.newInstance().
                    newTransformer().transform(new DOMSource(d.getDocumentElement()), 
                    r);

        } catch (Exception ex) {
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public static void writeDoc(Document d, File f) {
        try {
            TransformerFactory.newInstance().
                    newTransformer().transform(new DOMSource(d.getDocumentElement()), 
                    new StreamResult(f));

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public static String writeDoc(Document d ) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory.newInstance().
                    newTransformer().transform(new DOMSource(d.getDocumentElement()), 
                    new StreamResult(sw));
            return sw.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }

    }

    /**
     * Ein XML-Dokument auswerten mit einem XPATH-Ausdruck
     * @param d
     * @param exp
     * @return Nodelist
     */
    public static NodeList xp_nl(Document d, String exp) {
        try {
            return (NodeList) XPathFactory.newInstance().newXPath().evaluate(exp, d, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * * Ein XML-Dokument auswerten mit einem XPATH-Ausdruck. Geeignet, wenn nur EIN Wert zurückgegeben wird.
     * @param d
     * @param exp
     * @return String
     */
    public static String xp(Document d, String exp) {
        try {
            return XPathFactory.newInstance().newXPath().evaluate(exp, d);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        try {
            Document d = xmlParser.newDoc();
            d.appendChild(d.createElement("hallo"));
            System.out.println(xmlParser.writeDoc(d));
          
        } catch (Exception ex) {
            ex.printStackTrace();
            
            Logger.getLogger(xmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
