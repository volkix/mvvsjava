/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vshm.lib.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 006020x
 */
public class io {

    public static String getFileAsString(String filename) {
        try {
            return new String(getFileAsByteArray(filename));
        } catch (Exception ex) {
            Logger.getLogger(io.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public static void writeFileFromString(String filename, String daten) {
        writeFileFromByteArray(filename, daten.getBytes());
    }

    public static void appendFileFromString(String filename, String daten) {
        appendFileFromByteArray(filename, daten.getBytes());
    }

    public static byte[] getFileAsByteArray(String filename) {

        try {
            Path p = FileSystems.getDefault().getPath(filename);
            return Files.readAllBytes(p);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeFileFromByteArray(String filename, byte[] daten) {
        try {
            Path p = FileSystems.getDefault().getPath(filename);
            Files.write(p, daten, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void appendFileFromByteArray(String filename, byte[] daten) {
        try {

            Path p = FileSystems.getDefault().getPath(filename);
            Files.write(p, daten, StandardOpenOption.APPEND, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFileAsHEXStringForJS(String filename) {

        try {
            byte[] b = getFileAsByteArray(filename);
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < b.length; i++) {
                s.append(String.format("%%%02X", b[i]));
            }
            return s.toString();
        } catch (Exception iOException) {
            iOException.printStackTrace();
        }
        return "";
    }

    public static String getInputStreamAsString(InputStream filename) {
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer(4000);
        try {
            br = new BufferedReader(new InputStreamReader(filename, StandardCharsets.UTF_8));
            int str;
            while ((str = br.read()) != -1) {
                sb.append((char) str);
            }
            br.close();
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
