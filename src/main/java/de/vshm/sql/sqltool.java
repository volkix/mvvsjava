/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vshm.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import de.vshm.lib.json.JsonArray;
import de.vshm.lib.json.JsonObject;
import de.vshm.server.logger;

/**
 * Für Zugriff auf SQLITE-Datenbank
 *
 * @author 006020x
 */
public class sqltool {

    public static String driver = "org.sqlite.JDBC";

    static {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            logger.getInstance().getLogger().log(Level.ALL, "sqlite-driver nicht gefunden", ex);
        }
    }

 
    /**
     * kopiert einen resultset in ein jsonobject. vereinfacht die weiterverarbeitung
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public static JsonObject resultSetToJsonObject(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        JsonArray list = new JsonArray();

        while (rs.next()) {
            JsonObject row = new JsonObject();
            for (int i = 1; i <= columns; ++i) {
                JsonObject col = new JsonObject();
                col.add("val", rs.getString(i));
                col.add("typ", md.getColumnTypeName(i));
                row.add(md.getColumnName(i), col);
            }
            list.add(row);
        }
        return new JsonObject().add("resultset", list);
    }

 

    /**
     * nur für select-statements
     *
     * @param db_pfad
     * @param sql
     * @return
     */
    public static JsonObject exec_select_json(String db_pfad, String sql) {
        ResultSet rs = null;
        JsonObject ret = new JsonObject();
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + db_pfad, "sa", "");
            Statement stmt = conn.createStatement();
            if (sql.toLowerCase().contains("select")) {
                rs = stmt.executeQuery(sql);
                ret = resultSetToJsonObject(rs);
            }
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            logger.getInstance().getLogger().log(Level.ALL, "sql-fehler", ex);
        }
        return ret;
    }

    /**
     * für alle anderen statements
     *
     * @param db_pfad
     * @param sql
     * @return
     */
    public static int exec_sql(String db_pfad, String sql) {
        int ret = 9999;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + db_pfad, "sa", "");
            Statement stmt = conn.createStatement();
            ret = stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            logger.getInstance().getLogger().log(Level.ALL, "sql-fehler", ex);
        }
        return ret;
    }

}
