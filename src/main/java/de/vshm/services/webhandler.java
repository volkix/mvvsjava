/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vshm.services;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.vshm.lib.json.JsonObject;
import de.vshm.server.logger;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.logging.Level;

/**
 *
 * @author 006020x
 */
public class webhandler implements HttpHandler {

 
    private static final String MANDANT1 = "mandant";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATIONOCTETSTREAM = "application/octet-stream";
    private static final String HANDLERWEB_ = "handler:web: ";
    private static final String INDEXHTML = "index.html";
    private static final String SLASH = "/";
    private static final String WEBHANDLER_PFAD = "webhandler_pfad";
    private static final String MANDANT = "mandant";
    private static final String CONFIG = "config";
    private static final String HOST = "Host";

    @Override
    public void handle(HttpExchange he) throws IOException {
        try {

            String mandant = he.getRequestHeaders().getFirst(HOST);
            int doppelp = mandant.indexOf(":");
            if (doppelp > -1) {
                mandant = mandant.substring(0, doppelp);
            }
            JsonObject c = (JsonObject) he.getHttpContext().getAttributes().get(CONFIG);
            String ur = he.getRequestURI().toString();
            int fragezeichen = ur.indexOf("?");
            if (fragezeichen > 0) {
                ur = ur.substring(0, fragezeichen);
            }
            if (Files.isDirectory(Paths.get(c.get(MANDANT).asObject().get(mandant).asObject().get(WEBHANDLER_PFAD).asString() + ur)
            )) {
                if (!ur.endsWith(SLASH)) {
                    ur += SLASH;
                };
                ur += INDEXHTML;
            }
            logger.getInstance().getLogger().log(Level.INFO, HANDLERWEB_ + ur);
            he.getResponseHeaders().set(CONTENT_TYPE, APPLICATIONOCTETSTREAM);

            byte[] response = Files.readAllBytes(Paths.get(c.get(MANDANT1).asObject().get(mandant).asObject().get(WEBHANDLER_PFAD).asString() + ur));
            Iterator<JsonObject.Member> it = c.get("ct").asObject().iterator();
            boolean found = false;
            while (!found && it.hasNext()) {
                JsonObject.Member memb = it.next();

                if (ur.endsWith(memb.getName())) {
                    he.getResponseHeaders().set(CONTENT_TYPE, memb.getValue().asString());
                    found = true;
                }
            }
            he.sendResponseHeaders(200, response.length);
            OutputStream os = he.getResponseBody();
            os.write(response);
            os.flush();
            os.close();
            he.close();
        } catch (IOException iOException) {
            logger.getInstance().getLogger().log(Level.INFO, iOException.getMessage());
            he.close();
        }
    }

}
