
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vshm.services;

import de.vshm.server.baseapphandler;
import de.vshm.lib.json.JsonObject;
import de.vshm.sql.sqltool;

/**
 *
 * @author 006020x
 */
public class notizhandler extends baseapphandler {

    @Override
    public JsonObject execute() {

        JsonObject ret = super.execute();

        if (fehler) {
            return ret;
        }

        String aktion = getData().asObject().get("aktion").asString();
        if (aktion.equals("ns")) {
            return ns(ret);
        } else if (aktion.equals("nd")) {
            return nd(ret);
        } else if (aktion.equals("nu")) {
            return nu(ret);
        }
        else if (aktion.equals("nn")) {
            return nn(ret);
        }
        return ret;
    }

    private JsonObject ns(JsonObject ret) {
        // notizsuche
        String was = getData().asObject().get("request").asObject().get("was").asString();
        ret.add("data", sqltool.exec_select_json(getConfig().get("dbnotizen").asString(),
                "select rowid, daten from notizen where daten glob '" + was + "' order by substr(daten,1,8),rowid;"));
        return ret;
    }

    private JsonObject nd(JsonObject ret) {
        // notiz loeschen
        String id = getData().asObject().get("request").asObject().get("id").asString();
        ret.add("data", sqltool.exec_sql(getConfig().get("dbnotizen").asString(),
                "delete from notizen where rowid = " + id + " ;"));
        return ret;
    }

    private JsonObject nu(JsonObject ret) {
        // notiz update
        String was = getData().asObject().get("request").asObject().get("was").asString();
        String id = getData().asObject().get("request").asObject().get("id").asString();
        String wann = getData().asObject().get("request").asObject().get("wann").asString();
        String upd = wann + "::" + was;
        ret.add("data", sqltool.exec_sql(getConfig().get("dbnotizen").asString(),
                "update notizen set daten = '" + upd + "' where rowid = " + id + " ;"));
        return ret;
    }
    private JsonObject nn(JsonObject ret) {
        // notiz neu
        String was = getData().asObject().get("request").asObject().get("was").asString();
        String wann = getData().asObject().get("request").asObject().get("wann").asString();
        String ins = wann + "::" + was;
        ret.add("data", sqltool.exec_sql(getConfig().get("dbnotizen").asString(),
                "insert into notizen values ('" + ins + "');"));
        return ret;
    }

}
